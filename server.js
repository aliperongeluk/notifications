const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');
const mongo = require('./src/connections/mongodb.connection');

const express = require('express');
const app = express();
const port = process.env.PORT || 7010;

const notificationRoutes = require('./src/routes/notifications.routes');
const customerRoutes = require('./src/routes/customers.routes');
const messageReceiver = require('./src/eventhandlers/message.receiver');

const bodyParser = require('body-parser');

const rabbitmq = require('./src/connections/rabbitmq.connection');
const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/notifications', notificationRoutes);
app.use('/customers', customerRoutes);

// default:
app.use('*', (req, res) => {
  res.status(404).send({
    error: 'not found',
  });
});

rabbitmq
  .connect()
  .then(channels => {
    console.log('successfully connected with: RABBITMQ');
    messageReceiver.setMessageChannel(channels.receiveChannel);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

if (process.env.NODE_ENV === 'production') {
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
