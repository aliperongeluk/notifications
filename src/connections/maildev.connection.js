const nodemailer = require('nodemailer');

const sendMail = async (emailAddress, subject, message) => {
  const mailOptions = {
    from: 'aliservice@ali.nl',
    to: emailAddress,
    subject: subject,
    text: message,
  };

  const transporter = nodemailer.createTransport({
    host: 'maildev',
    port: 25,
    secure: false,
    ignoreTLS: true,
  });

  transporter.verify((err, success) => {
    if (err) {
      console.error('ERROR: ' + err);
    } else {
      console.log('SUCCESS: ' + success);
    }
  });

  try {
    await transporter.sendMail(mailOptions);
    console.log('send mail!');
  } catch (err) {
    console.log(err);
    throw 'Failed to send mail';
  }
};

module.exports = { sendMail };
