const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Notification:
 *    type: object
 *    properties:
 *      _id:
 *        type: string
 *      name:
 *        type: string
 *      message:
 *        type: string
 *      notificationChannel:
 *        type: array
 *        enum: [EMAIL, SMS, LETTER]
 *        items:
 *          type: string
 *      customerInformation:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          email:
 *            type: string
 *          phone:
 *            type: number
 */
const NotificationSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
    notificationChannel: [
      {
        type: String,
        enum: ['EMAIL', 'SMS', 'LETTER'],
        required: true,
      },
    ],
    customerInformation: {},
  },
  {
    timestamps: true,
  }
);

const Notification = mongoose.model('notification', NotificationSchema);

module.exports = Notification;
