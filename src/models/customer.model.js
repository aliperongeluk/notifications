const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CustomerSchema = new Schema(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: [
        true,
        'An id from the customer information microservice is required',
      ],
    },
    authId: {
      type: Schema.Types.ObjectId,
      required: [
        true,
        'An id from the authentication microservice is required',
      ],
    },
    email: {
      type: String,
      required: [true, 'Email is required'],
    },
    phoneNumber: {
      type: String,
    },
  },
  {
    timestamps: true,
    _id: false,
  }
);

const Customer = mongoose.model('customer', CustomerSchema);

module.exports = Customer;
