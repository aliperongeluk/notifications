const express = require('express');
const routes = express.Router();
const Customer = require('../models/customer.model');

routes.get('/', (req, res) => {
  Customer.find({})
    .then(notifications => {
      res.status(200).send(notifications);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
