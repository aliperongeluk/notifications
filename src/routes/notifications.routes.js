const express = require('express');
const routes = express.Router();
const Notification = require('../models/notification.model');
const Customer = require('../models/customer.model');

/**
 * @swagger
 * /api/notification-management/notifications/:
 *    get:
 *     tags:
 *       - notifications
 *     description: Returns all notifications made
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Successfully loaded customers.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Notification'
 *       400:
 *          description: Failed to load customers.
 */
routes.get('/', (req, res) => {
  Notification.find({})
    .then(notifications => {
      res.status(200).send(notifications);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/notification-management/notifications/id/:
 *    get:
 *     tags:
 *       - notifications
 *     description: Returns a specific notification
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *             type: string
 *         required: true
 *         description: Object ID of the notification
 *     responses:
 *       200:
 *          description: Successfully loaded a notification
 *          schema:
 *              $ref: '#/definitions/Notification'
 *       400:
 *          description: Failed to load a notification
 */
routes.get('/:id', (req, res) => {
  const id = req.params._id;

  Notification.findOne({ _id: id })
    .then(notification => {
      res.status(200).send(notification);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/notification-management/notifications/:
 *    post:
 *      tags:
 *        - notifications
 *      description: Create a notification
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: body
 *          description: body-attributes to make a notification
 *          schema:
 *              $ref: '#/definitions/Notification'
 *      responses:
 *        200:
 *          description: Added notification successfully.
 *          schema:
 *              $ref: '#/definitions/Notification'
 *        400:
 *          description: Failed to create a notification.
 */
routes.post('/', (req, res) => {
  const notification = new Notification(req.body);

  notification
    .save()
    .then(() => {
      res.status(200).json({ success: 'successfully added a notification' });
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

routes.get('/users', (req, res) => {
  Customer.find({})
    .then(notifications => {
      res.status(200).send(notifications);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
