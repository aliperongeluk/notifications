const { constants, events } = require('../../environment/env');
const sendMail = require('../../connections/maildev.connection');
const Customer = require('../../models/customer.model');

const consumeInvoiceCreated = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.INVOICE_CREATED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.INVOICE_EXCHANGE,
        events.INVOICE_CREATED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.INVOICE_CREATED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeInvoiceCreated(messageChannel);
};

module.exports = { consume };
