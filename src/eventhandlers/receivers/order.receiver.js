const { constants, events } = require('../../environment/env');
const sendMail = require('../../connections/maildev.connection');
const Customer = require('../../models/customer.model');

const consumeOrderCreated = messageChannel => {
  messageChannel
    .assertQueue(`${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_CREATED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_CREATED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.ORDER_CREATED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderPending = messageChannel => {
  messageChannel
    .assertQueue(`${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_PENDING}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_PENDING
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.ORDER_PENDING,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderCancelled = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_CANCELLED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_CANCELLED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.ORDER_CANCELLED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderPicked = messageChannel => {
  messageChannel
    .assertQueue(`${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_PICKED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_PICKED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(customer.email, events.ORDER_PICKED, message);
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderSent = messageChannel => {
  messageChannel
    .assertQueue(`${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_SENT}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_SENT
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(customer.email, events.ORDER_SENT, message);
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderReceived = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_RECEIVED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_RECEIVED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.ORDER_RECEIVED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consumeOrderReturned = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.ORDER_RETURNED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.ORDER_EXCHANGE,
        events.ORDER_RETURNED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.ORDER_RETURNED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

const consume = messageChannel => {
  consumeOrderCreated(messageChannel);
  consumeOrderPending(messageChannel);
  consumeOrderCancelled(messageChannel);
  consumeOrderPicked(messageChannel);
  consumeOrderSent(messageChannel);
  consumeOrderReceived(messageChannel);
  consumeOrderReturned(messageChannel);
};

module.exports = { consume };
