const { constants, events } = require('../../environment/env');
const Customer = require('../../models/customer.model');
const sendMail = require('../../connections/maildev.connection');

const consumeRegistrations = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.USER_REGISTERED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.USER_EXCHANGE,
        events.USER_REGISTERED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const customer = new Customer(JSON.parse(message.content));
          console.log(customer);
          await customer.save();

          await sendMail.sendMail(
            customer.email,
            events.USER_REGISTERED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consumeUserUpdates = messageChannel => {
  messageChannel
    .assertQueue(`${constants.NOTIFICATIONS_SERVICE}_${events.USER_UPDATED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.USER_EXCHANGE,
        events.USER_UPDATED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const messageObject = JSON.parse(message.content);

          const customer = await Customer.findByIdAndUpdate(
            messageObject._id,
            messageObject,
            {
              new: true,
            }
          );

          await sendMail.sendMail(customer.email, events.USER_UPDATED, message);
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeRegistrations(messageChannel);
  consumeUserUpdates(messageChannel);
};

module.exports = { consume };
