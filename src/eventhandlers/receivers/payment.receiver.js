const { constants, events } = require('../../environment/env');
const sendMail = require('../../connections/maildev.connection');
const Customer = require('../../models/customer.model');

const consumePaymentCompleted = messageChannel => {
  messageChannel
    .assertQueue(
      `${constants.NOTIFICATIONS_SERVICE}_${events.PAYMENT_COMPLETED}`,
      {
        exclusive: false,
      }
    )
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.PAYMENT_EXCHANGE,
        events.PAYMENT_COMPLETED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const id = await JSON.parse(message.content).customerInformationId;
          const customer = await Customer.findById(id);

          await sendMail.sendMail(
            customer.email,
            events.PAYMENT_COMPLETED,
            message
          );
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumePaymentCompleted(messageChannel);
};

module.exports = { consume };
