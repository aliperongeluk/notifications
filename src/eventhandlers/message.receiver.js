const { constants } = require('../environment/env');

const invoiceConsumers = require('./receivers/invoice.receiver');
const orderConsumers = require('./receivers/order.receiver');
const paymentConsumers = require('./receivers/payment.receiver');
const userConsumers = require('./receivers/user.receiver');

let messageChannel;

const startConsuming = () => {
  createExchanges();
  invoiceConsumers.consume(messageChannel);
  orderConsumers.consume(messageChannel);
  paymentConsumers.consume(messageChannel);
  userConsumers.consume(messageChannel);
};

const createExchanges = () => {
  messageChannel.assertExchange(constants.INVOICE_EXCHANGE, 'topic', {
    durable: true,
  });
  messageChannel.assertExchange(constants.ORDER_EXCHANGE, 'topic', {
    durable: true,
  });
  messageChannel.assertExchange(constants.PAYMENT_EXCHANGE, 'topic', {
    durable: true,
  });
  messageChannel.assertExchange(constants.USER_EXCHANGE, 'topic', {
    durable: true,
  });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };
